namespace Jokee.Utils.Models;

public class BaseSetting
{
    public BaseConfig Config { get; set; }
}

public class BaseConfig
{
    public BaseDatabase Dbs { get; set; }
    public string ENV { get; set; }

}

public partial class BaseDatabase
{
    public bool CreateDataBase { get; set; }
    public bool Seeding { get; set; }
    public bool Clear { get; set; }
    public bool Migrate { get; set; }
    public Dictionary<string, Dictionary<string, string>> ConnectionString { get; set; }
}

public class BaseServer
{
    public BaseHttp Http { get; set; }
    public BaseHttps Https { get; set; }
}

public class BaseHttp
{
    public string IPAddress { get; set; }
    public int Port { get; set; }
}

public class BaseHttps
{
    public string IPAddress { get; set; }
    public int Port { get; set; }
    public string Certificat { get; set; }
    public string Password { get; set; }
}