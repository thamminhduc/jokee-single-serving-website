using Jokee.DAL.Dbs;
using Jokee.DAL.UnitOfWork;
using JokeeService.Mapping;
using JokeeService.Model;
using JokeeService.Service.IService;

namespace JokeeService.Service;

public class JokeService : IJokeService
{
    private UnitOfWork _unitOfWork;

    public JokeService(AppDbContext dbContext)
    {
        _unitOfWork = new UnitOfWork(dbContext);
    }
    public async Task<JokeResponse> GetRandomJoke(List<Guid> jokesThatUserReacted)
    {
        var jokeDto = await _unitOfWork.JokeRepositoty.GetRandomJoke(jokesThatUserReacted);

        return jokeDto.ToModelResponse();
    }
}