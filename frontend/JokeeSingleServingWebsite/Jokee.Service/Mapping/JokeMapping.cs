using Jokee.DAL.Dbs;
using JokeeService.Model;

namespace JokeeService.Mapping;

public static class JokeMapping
{
    public static JokeResponse ToModelResponse(this joke joke)
    {
        if (joke == null) return new JokeResponse();
        return new JokeResponse()
        {
            Id = joke.id,
            Content = joke.content
        };;
    }
}