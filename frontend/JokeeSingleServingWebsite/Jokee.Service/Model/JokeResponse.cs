namespace JokeeService.Model;

public class JokeResponse
{
    public Guid Id { get; set; }
    public  string Content { get; set; }
}