using Jokee.Utils.Models;

namespace Jokee.Utils;

public static class Constant
{
    public static void Init(BaseSetting appSettings)
    {
        AppSettings = appSettings;
        ENV = appSettings.Config.ENV;
        if (AppSettings.Config != null)
        {
            Config = appSettings.Config;
        }
    }

    public static BaseSetting AppSettings { get; set; }
    public static BaseConfig Config { get; set; }
    public static string ENV { get; set; }
    public static string CookieJokeThatUserReacted = "JOKE_THAT_USER_REACTED";

}