﻿$(document).ready(function () {
    $('.btn').click(function () {
        var jokeId = $('.content').data('id');
        if(jokeId !== "00000000-0000-0000-0000-000000000000")
            reactJoke(jokeId);
    });
})

function reactJoke(jokeId) {
    $.ajax({
        url: `/Ajax/ReactJoke`,
        type: 'GET',
        cache: false,
        data: {
            jokeId: jokeId,
        },
        success: function (res) {
            $('.content-wrapper').empty().append(res);
        },
        error: function () {
            console.log("Lỗi gọi action");
        },
        complete: function () {

        },
    });
}
