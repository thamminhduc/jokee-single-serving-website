using Jokee.DAL.Dbs;
using Jokee.DAL.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Jokee.DAL.Repository;

public class JokeRepository : GenericRepository<joke>, IJokeRepository
{
    private readonly AppDbContext _dbContext;
    public JokeRepository(AppDbContext dbContext) : base(dbContext)
    {
        _dbContext = context;
    }

    public async Task<joke> GetRandomJoke(List<Guid> jokesThatUserReacted)
    {
        var joke = await _dbContext.Jokes
            .FirstOrDefaultAsync(j => !jokesThatUserReacted.Contains(j.id));

        return joke;
    }
}