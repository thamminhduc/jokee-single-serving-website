using System.Net;
using Jokee.DAL.Dbs;
using Jokee.Utils;
using JokeeService.Service;
using JokeeService.Service.IService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JokeeService;

public static class ConfigureService
{
    public static IServiceCollection AddJokeeService(this IServiceCollection services)
        {
            int maxRetryCount = 3;
            TimeSpan maxRetryDelay = TimeSpan.FromSeconds(10);
            if (Constant.ENV.Equals("PROD"))
            {
            }
            else
            {
                services.AddDbContext<AppDbContext>(opts =>
                {
                    opts.UseNpgsql(Constant.Config.Dbs.ConnectionString["app"]["ro"], options =>
                    {
                        options.EnableRetryOnFailure(maxRetryCount, maxRetryDelay, new List<string>());
                    });
                });
            }
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            services.AddTransient<IJokeService, JokeService>();
            return services;
        }
}