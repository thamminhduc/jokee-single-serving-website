using Jokee.DAL.Dbs;
using Jokee.DAL.Repository;

namespace Jokee.DAL.UnitOfWork;

public class UnitOfWork : IDisposable
{
    private readonly AppDbContext _context;
    private JokeRepository _jokeRepository;
    public JokeRepository JokeRepositoty
    {
        get
        {
            if (this._jokeRepository == null)
            {
                this._jokeRepository = new JokeRepository(_context);
            }
            return _jokeRepository;
        }
    }

    public UnitOfWork(AppDbContext dbContext)
    {
        _context = dbContext;
    }
    

    public void Save()
    {
        _context.SaveChanges();
    }

    private bool disposed = false;

    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposed)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
        this.disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}