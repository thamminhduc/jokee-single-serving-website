using Jokee.Utils;
using JokeeService.Model;
using JokeeService.Service.IService;
using Microsoft.AspNetCore.Mvc;

namespace JokeeSingleServingWebsite.Controllers
{
    public class AjaxController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IJokeService _jokeService;

        public AjaxController(ILogger<HomeController> logger,
            IJokeService jokeService)
        {
            _logger = logger;
            _jokeService = jokeService;
        }

        public async Task<IActionResult> ReactJoke(Guid jokeId)
        {
            var jokesThatUserReacted = new List<Guid>();
            var jokeResponse = new JokeResponse();
            var cookie = HttpContext.Request.Cookies[Constant.CookieJokeThatUserReacted];
            if (string.IsNullOrEmpty(cookie))
                cookie = string.Empty;
            cookie = $"{cookie},{jokeId.ToString()}";
            HttpContext.Response.Cookies.Append(Constant.CookieJokeThatUserReacted, cookie);
            var guidArr = cookie.Split(",");
            jokesThatUserReacted = jokesThatUserReacted = guidArr.Select(g =>
            {
                if (!string.IsNullOrEmpty(g))
                {
                    return Guid.Parse(g);
                }

                return Guid.Empty;
            }).ToList();
            var joke = await _jokeService.GetRandomJoke(jokesThatUserReacted);
            if (joke != null)
            {
                jokeResponse = joke;
            }

            return PartialView("_jokeContentPartial", jokeResponse);
        }
    }
}