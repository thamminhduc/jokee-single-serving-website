﻿// See https://aka.ms/new-console-template for more information

var input = string.Empty;

input = Console.ReadLine();

if (input != null)
{
    var array = input.Split(" ").Select(long.Parse).ToArray();
    Console.WriteLine(MinMaxSum(array));
}
string MinMaxSum(long[] array)
{
    long minSum = 0;
    long maxSum = 0;
    Array.Sort(array);
    var len = array.Length;
    var j = len - 1;
    for (int i = 0; i < len - 1; i++)
    {
        minSum += array[i];
        maxSum += array[j];
        j--;
    }
    return $"{minSum} {maxSum}";
}