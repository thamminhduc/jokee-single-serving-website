using Microsoft.Extensions.Configuration;
using System.IO;


namespace Jokee.Utils;
public class AppSettingsHelper
{
    private static string ConfFile { get; set; }
    public static IConfigurationRoot GetAppSettings(string filename)
    {
        var appsettings = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(filename, optional: false)
            .Build();
        ConfFile = appsettings.GetSection("ConfigFile").Value;
        return appsettings;
    }
    public static IConfigurationRoot BuildSettings(string filename)
    {
        var config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(filename, optional: false).Build();
        return config;
    }
    public static IConfigurationRoot BuildApplicationSettings()
    {
        if (!string.IsNullOrEmpty(ConfFile)) 
        { 
            ConfFile = "Settings/dev.json"; 
        }
        var config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(ConfFile, optional: false).Build();
        return config;
    }

    public static IConfigurationRoot BuildErrorCode()
    {
        ConfFile = "Settings/ExceptionCode.json";
        var config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(ConfFile, optional: false).Build();
        return config;
    }
}