using System.Data;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npgsql;

namespace Jokee.DAL.Dbs;

public partial class AppDbContext : DbContext
{
    public AppDbContext()
    {
    }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected AppDbContext(DbContextOptions options)
        : base(options)
    {
    }

    public virtual DbSet<joke> Jokes { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
        }
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<joke>(entity =>
        {
            entity.ToTable("joke");

            entity.HasIndex(e => e.id, "IX_Joke_Id");

            entity.HasIndex(e => e.content, "IX_Account_NormalizedAddress");

            entity.Property(e => e.id).ValueGeneratedNever();

            entity.Property(e => e.content);
        });


        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    
    
    
    
    public async Task<T> ExcFunctionCursorAsync<T>(string functionName, params object[]? param)
    {
        using NpgsqlConnection conn = new NpgsqlConnection(this.Database.GetConnectionString());
        var watch = new Stopwatch();
        watch.Start();

        conn.Open();
        NpgsqlTransaction tran = conn.BeginTransaction();
        try
        {

            NpgsqlCommand command = new NpgsqlCommand(functionName, conn);
            command.CommandType = CommandType.StoredProcedure;

            if (param != null)
            {
                foreach (var vl in param)
                {
                    if (vl != null)
                        command.Parameters.AddWithValue(vl);
                }
            }

            await command.ExecuteNonQueryAsync();

            command.CommandText = "FETCH ALL FROM \"mycursor\";";
            command.CommandType = CommandType.Text;

            NpgsqlDataReader dr = await command.ExecuteReaderAsync();
            var dataTable = new DataTable();
            dataTable.Load(dr);
            dr.Close();

            if (dataTable.Rows.Count > 0)
            {
                var serializedMyObjects = JsonConvert.SerializeObject(dataTable);



                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
                };

                var myObjects = JsonConvert.DeserializeObject<T>(serializedMyObjects, settings);

                tran.Commit();
                conn.Close();
                watch.Stop();

                return myObjects;
            }
        }
        catch (Exception ex)
        {
            //_logger.LogError(ex.Message);
            var strParam = "";
            if(param != null)
            {
                foreach (var item in param)
                {
                    strParam += $"{item}, ";
                }
            }
            if (!string.IsNullOrEmpty(strParam) && strParam.Trim().EndsWith(','))
                strParam = strParam.Trim().Substring(0, strParam.Length - 1);
            Console.WriteLine(Environment.MachineName, "", "FunctionName: " + functionName + " - Ex: " + ex.Message);
            conn.Close();
        }
        watch.Stop();
        return default(T);
    }

    public async Task<DataRowCollection?> ExcFunctionCursorWithoutMappingAsync(string functionName, params object[]? param)
    {
        using NpgsqlConnection conn = new NpgsqlConnection(this.Database.GetConnectionString());
        var watch = new Stopwatch();
        watch.Start();

        conn.Open();
        NpgsqlTransaction tran = conn.BeginTransaction();
        try
        {

            NpgsqlCommand command = new NpgsqlCommand(functionName, conn);
            command.CommandType = CommandType.StoredProcedure;

            if (param != null)
            {
                foreach (var vl in param)
                {
                    if (vl != null)
                        command.Parameters.AddWithValue(vl);
                }
            }

            await command.ExecuteNonQueryAsync();

            command.CommandText = "FETCH ALL FROM \"mycursor\";";
            command.CommandType = CommandType.Text;

            NpgsqlDataReader dr = await command.ExecuteReaderAsync();
            var dataTable = new DataTable();

            dataTable.Clear();
            dataTable.Load(dr);
            dr.Close();

            if (dataTable.Rows.Count > 0)
            {
                var dataRow = dataTable.Rows;
                return dataRow;
            }
        }
        catch (Exception ex)
        {
            //_logger.LogError(ex.Message);
            var strParam = "";
            foreach (var item in param)
            {
                strParam += $"{item}, ";
            }
            if (!string.IsNullOrEmpty(strParam) && strParam.Trim().EndsWith(','))
                strParam = strParam.Trim().Substring(0, strParam.Length - 1);
            Console.WriteLine(Environment.MachineName, "", "FunctionName: " + functionName + " - Ex: " + ex.Message);
            conn.Close();
        }
        watch.Stop();
        return null;
    }

    public async Task<int> ExecuteFunctionNonQueryAsync(string functionName, Dictionary<string, dynamic> prams)
    {
        using NpgsqlConnection conn = new NpgsqlConnection(this.Database.GetConnectionString());
        var watch = new Stopwatch();
        watch.Start();

        try
        {
            conn.Open();
            NpgsqlTransaction tran = conn.BeginTransaction();

            NpgsqlCommand command = new NpgsqlCommand(functionName, conn);
            command.CommandType = CommandType.StoredProcedure;

            foreach (var propertyInfo in prams)
            {
                command.Parameters.AddWithValue(propertyInfo.Key, propertyInfo.Value);
            }

            var rs = await command.ExecuteScalarAsync();

            tran.Commit();
            conn.Close();
            watch.Stop();

            return Convert.ToInt32(rs);
        }
        catch (Exception ex)
        {
            //_logger.LogError(ex.Message);
            var strParam = "";
            foreach (var propertyInfo in prams)
            {
                strParam += $"{propertyInfo.Key}: {propertyInfo.Value}, ";
            }
            if (!string.IsNullOrEmpty(strParam) && strParam.Trim().EndsWith(','))
                strParam = strParam.Trim().Substring(0, strParam.Length - 1);
            Console.WriteLine(Environment.MachineName, "", "FunctionName: " + functionName + " - Ex: " + ex.Message);
            conn.Close();
        }

        watch.Stop();
        return -1;
    }

}