using JokeeService.Model;

namespace JokeeService.Service.IService;

public interface IJokeService
{
    public Task<JokeResponse> GetRandomJoke(List<Guid> jokesThatUserReacted);
}