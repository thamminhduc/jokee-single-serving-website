using JokeeService.Model;

namespace JokeeSingleServingWebsite.Models;

public class HomeVm
{
    public JokeResponse Joke { get; set; }
}