using Jokee.DAL.Dbs;

namespace Jokee.DAL.Repository.IRepository;

public interface IJokeRepository
{
    public Task<joke> GetRandomJoke(List<Guid> jokesThatUserReacted);
}