﻿using System.Diagnostics;
using Jokee.Utils;
using JokeeService.Service.IService;
using Microsoft.AspNetCore.Mvc;
using JokeeSingleServingWebsite.Models;

namespace JokeeSingleServingWebsite.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IJokeService _jokeService;
    public HomeController(ILogger<HomeController> logger,
        IJokeService jokeService)
    {
        _logger = logger;
        _jokeService = jokeService;
    }

    public async Task<IActionResult> Index()
    {
        var homeVm = new HomeVm();
        try
        {
            var jokesThatUserReacted = new List<Guid>();
            var cookie = HttpContext.Request.Cookies[Constant.CookieJokeThatUserReacted];
            if (!string.IsNullOrEmpty(cookie))
            {
                var guidArr = cookie.Split(",");
                jokesThatUserReacted = guidArr.Select(g =>
                {
                    if (!string.IsNullOrEmpty(g))
                    {
                        return Guid.Parse(g);
                    }
                    return Guid.Empty;
                }).ToList();
            }
            var joke = await _jokeService.GetRandomJoke(jokesThatUserReacted);
            if (joke != null)
            {
                homeVm.Joke = joke;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        return View(homeVm);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}