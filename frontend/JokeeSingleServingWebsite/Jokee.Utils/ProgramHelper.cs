using System.Reflection.Metadata;
using Jokee.Utils.Models;
using Microsoft.Extensions.Configuration;

namespace Jokee.Utils;

public class ProgramHelper
{
    public static void Init()
    {
        var appSettings = new BaseSetting();
        AppSettingsHelper.GetAppSettings("appsettings.json");
        IConfigurationRoot appjson = AppSettingsHelper.BuildApplicationSettings();
        appjson.Bind(appSettings);
        Constant.Init(appSettings);
    }
}